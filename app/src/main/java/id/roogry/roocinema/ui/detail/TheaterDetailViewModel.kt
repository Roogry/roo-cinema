package id.roogry.roocinema.ui.detail

import android.app.Application
import androidx.lifecycle.ViewModel
import id.roogry.roocinema.database.Theater
import id.roogry.roocinema.repository.TheaterRepository

class TheaterDetailViewModel(application: Application) : ViewModel() {
    private val theaterRepository: TheaterRepository = TheaterRepository(application)
    fun delete(theater: Theater) {
        theaterRepository.delete(theater)
    }
}