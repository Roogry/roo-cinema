package id.roogry.roocinema.ui.insert

import android.app.Application
import androidx.lifecycle.ViewModel
import id.roogry.roocinema.database.Theater
import id.roogry.roocinema.repository.TheaterRepository

class TheaterAddUpdateViewModel(application: Application) : ViewModel() {
    private val theaterRepository: TheaterRepository = TheaterRepository(application)
    fun insert(theater: Theater) {
        theaterRepository.insert(theater)
    }
    fun update(theater: Theater) {
        theaterRepository.update(theater)
    }
}