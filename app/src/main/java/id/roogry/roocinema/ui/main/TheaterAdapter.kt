package id.roogry.roocinema.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.roogry.roocinema.database.Theater
import id.roogry.roocinema.databinding.ItemTheaterBinding
import id.roogry.roocinema.helper.TheaterDiffCallback
import id.roogry.roocinema.ui.detail.TheaterDetailActivity

class TheaterAdapter : RecyclerView.Adapter<TheaterAdapter.TheaterViewHolder>() {
    private val listTheaters = ArrayList<Theater>()
    fun setListTheaters(listTheaters: List<Theater>) {
        val diffCallback = TheaterDiffCallback(this.listTheaters, listTheaters)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.listTheaters.clear()
        this.listTheaters.addAll(listTheaters)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TheaterViewHolder {
        val binding = ItemTheaterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TheaterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TheaterViewHolder, position: Int) {
        holder.bind(listTheaters[position])
    }

    override fun getItemCount(): Int {
        return listTheaters.size
    }

    inner class TheaterViewHolder(private val binding: ItemTheaterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(theater: Theater) {
            with(binding) {
                tvItemTitle.text = "Theater ${theater.name}"
                tvItemCapacity.text = theater.capacity.toString()
                tvItemDescription.text = "Fasilitas : ${theater.facility}"

                cvItemTheater.setOnClickListener {
                    val intent = Intent(it.context, TheaterDetailActivity::class.java)
                    intent.putExtra(TheaterDetailActivity.EXTRA_THEATER, theater)
                    it.context.startActivity(intent)
                }
            }
        }
    }
}