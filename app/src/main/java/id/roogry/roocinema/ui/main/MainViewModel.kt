package id.roogry.roocinema.ui.main

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import id.roogry.roocinema.database.Theater
import id.roogry.roocinema.repository.TheaterRepository

class MainViewModel (application: Application) : ViewModel() {
    private val theaterRepository: TheaterRepository = TheaterRepository(application)
    fun getAllTheaters(): LiveData<List<Theater>> = theaterRepository.getAllTheaters()
}