package id.roogry.roocinema.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.roogry.roocinema.R
import id.roogry.roocinema.databinding.ActivityMainBinding
import id.roogry.roocinema.helper.ViewModelFactory
import id.roogry.roocinema.ui.insert.TheaterAddUpdateActivity

class MainActivity : AppCompatActivity() {
    private var activityMainBinding: ActivityMainBinding? = null
    private val binding get() = activityMainBinding
    private lateinit var adapter: TheaterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        val mainViewModel = obtainViewModel(this)
        mainViewModel.getAllTheaters().observe(this, { theaterList ->
            adapter.setListTheaters(theaterList)

            if (theaterList.isNotEmpty()) {
                binding?.emptyHolder?.visibility = View.GONE
            }else{
                binding?.emptyHolder?.visibility = View.VISIBLE
            }
        })

        adapter = TheaterAdapter()

        binding?.rvTheaters?.layoutManager = LinearLayoutManager(this)
        binding?.rvTheaters?.setHasFixedSize(true)
        binding?.rvTheaters?.adapter = adapter

        binding?.fabAdd?.setOnClickListener { view ->
            if (view.id == R.id.fab_add) {
                val intent = Intent(this, TheaterAddUpdateActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun obtainViewModel(activity: AppCompatActivity): MainViewModel {
        val factory = ViewModelFactory.getInstance(activity.application)
        return ViewModelProvider(activity, factory).get(MainViewModel::class.java)
    }

    override fun onDestroy() {
        super.onDestroy()
        activityMainBinding = null
    }
}