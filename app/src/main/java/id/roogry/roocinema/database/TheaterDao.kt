package id.roogry.roocinema.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TheaterDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(theater: Theater)
    @Update
    fun update(theater: Theater)
    @Delete
    fun delete(theater: Theater)
    @Query("SELECT * from theater ORDER BY id ASC")
    fun getAllTheaters(): LiveData<List<Theater>>
}