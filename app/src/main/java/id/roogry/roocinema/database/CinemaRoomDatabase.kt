package id.roogry.roocinema.database

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteOpenHelper

@Database(entities = [Theater::class], version = 1)
abstract class CinemaRoomDatabase : RoomDatabase() {
    abstract fun theaterDao(): TheaterDao

    companion object {
        @Volatile
        private var INSTANCE: CinemaRoomDatabase? = null
        @JvmStatic
        fun getDatabase(context: Context): CinemaRoomDatabase {
            if (INSTANCE == null) {
                synchronized(CinemaRoomDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        CinemaRoomDatabase::class.java, "cinema_database")
                        .build()
                }
            }
            return INSTANCE as CinemaRoomDatabase
        }
    }

    override fun createOpenHelper(config: DatabaseConfiguration?): SupportSQLiteOpenHelper {
        TODO("Not yet implemented")
    }

    override fun createInvalidationTracker(): InvalidationTracker {
        TODO("Not yet implemented")
    }

    override fun clearAllTables() {
        TODO("Not yet implemented")
    }
}