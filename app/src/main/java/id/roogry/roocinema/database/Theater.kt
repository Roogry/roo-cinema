package id.roogry.roocinema.database

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
class Theater(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0,

    @ColumnInfo(name = "name")
    var name: String? = null,

    @ColumnInfo(name = "room_type")
    var roomType: String? = null,

    @ColumnInfo(name = "facility")
    var facility: String? = null,

    @ColumnInfo(name = "capacity")
    var capacity: Int? = null,
) : Parcelable