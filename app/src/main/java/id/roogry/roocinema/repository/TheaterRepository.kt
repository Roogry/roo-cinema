package id.roogry.roocinema.repository

import android.app.Application
import androidx.lifecycle.LiveData
import id.roogry.roocinema.database.Theater
import id.roogry.roocinema.database.TheaterDao
import id.roogry.roocinema.database.CinemaRoomDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class TheaterRepository(application: Application) {
    private val theatersDao: TheaterDao
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        val db = CinemaRoomDatabase.getDatabase(application)
        theatersDao = db.theaterDao()
    }

    fun getAllTheaters(): LiveData<List<Theater>> = theatersDao.getAllTheaters()
    fun insert(theater: Theater) {
        executorService.execute { theatersDao.insert(theater) }
    }
    fun delete(theater: Theater) {
        executorService.execute { theatersDao.delete(theater) }
    }
    fun update(theater: Theater) {
        executorService.execute { theatersDao.update(theater) }
    }
}